# BAVE Project

----------------------
How to run the project 
----------------------

***********************************

## Prerequisites

- NodeJS
- Unity 5.5
- Android SDK 6.0
- JDK 8

- Surface2TUIO
- TUIOClient

***********************************

## Database (MongoDB)

### Launch MongoDB

- `sudo service mongod stop`
- `sudo mongod`

### Import prepared database

- `cd server`
- `./import_db.sh`

***********************************

## Server (NodeJS)

- `cd server`
- `npm install`
- `node index.js`

***********************************

## Table (Unity)

### TUIO

- Launch Surface2TUIO.exe
- Launch TUIOClient `npm run develop`

### Project

- Set the url of the distant server in SocketIO scene object.
- Generate executable in Unity interface File > Build Settings 
- Select the scene and build for Windows
- Build and Launch it !

***********************************

## VR

- Connect the phone to the computer
- Set the url of the distant server in SocketIO scene object.
- Generate executable File > Build Settings
- Select the scene and build for Android
- Player Settings
- Other Settings > Bundle Identifier --> set the value by anything else
- Other Settings > Virtual Reality Supported --> check
- Build and launch it !
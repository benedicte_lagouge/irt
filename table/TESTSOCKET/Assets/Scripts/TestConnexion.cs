﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

public class TestConnexion : MonoBehaviour
{
	private SocketIOComponent socket;

	public void Start() 
	{
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		socket.On("connect", onConnect);
		socket.On("position", onPosition);
	

		Debug.Log("Launching routine...");	
		StartCoroutine("Routine");
	}

	private IEnumerator Routine()
	{
		Debug.Log("Routine begins...");
		
		// wait 1 seconds and continue
		yield return new WaitForSeconds(1);
		
		// Register
		Dictionary<string, string> data = new Dictionary<string, string>();
       	data["type"] = "table";

		Debug.Log("Registering...");

		socket.Emit("register",new JSONObject(data));

		yield return new WaitForSeconds(1);

		Dictionary<string, string> object1 = new Dictionary<string, string>();
       	object1["id_object"] = "1";
       	object1["type_category"] = "home";
		object1["type_name"] = "blue_home";
		object1["position_x"] = "1.07";
		object1["position_y"] = "20.92";
		object1["position_z"] = "-2.08";
		object1["angle"] = "90.0";
		object1["scale"] = "2.0";

		Debug.Log("Sending object...");
		
		socket.Emit("object",new JSONObject(object1));
	}

	public void onConnect(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Coonect received: " + e.name + " " + e.data);
	}
	
	public void onPosition(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Position received: " + e.name + " " + e.data);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;

public class Register : MonoBehaviour {

	private SocketIOComponent socket;

	public GameObject populateButton;
	public GameObject hailButton;
	public GameObject presentButton;
	public GameObject timerButton;

	private Text logText;
	
	public void Start() 
	{
		setActiveBtn (false);

		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();

		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();
		
		socket.On("connect", onConnect);
	}
	
	private IEnumerator RegisterRoutine()
	{
		// wait 1 seconds and continue
		yield return new WaitForSeconds(1);
		
		// Register
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["type"] = "table";
		
		socket.Emit("register",new JSONObject(data));
	}
	
	public void onConnect(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Connect received: " + e.name + " " + e.data);
		appLog ("Connected to the server");

		setActiveBtn (true);

		StartCoroutine("RegisterRoutine");
	}

	public void setActiveBtn(bool b)
	{
		populateButton.SetActive (b);
		hailButton.SetActive (b);
		presentButton.SetActive (b);
		timerButton.SetActive (b);
	}
	
	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}
}

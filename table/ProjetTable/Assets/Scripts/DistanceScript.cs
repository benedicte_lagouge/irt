﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;

public class DistanceScript : MonoBehaviour {

	private SocketIOComponent socket;

	public GameObject timerBtn;
	private TimerScript timerObject;

	public GameObject player;
	public GameObject target;

	public float distance; 
	public bool found;

	public GameObject giftImg;

	private Text label;
	private float previousDistance;

	private Text logText;
	private int offset = 0;

	// Use this for initialization
	void Start () {
		giftImg.SetActive (false);

		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();

		timerObject = timerBtn.GetComponent<TimerScript>();

		label = GameObject.Find ("Distance").GetComponent<UnityEngine.UI.Text>();

		player = null;
		target = null;
		found = false;
		previousDistance = 0.0f;
		distance = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null) {
			computeDistance ();
		}
	}

	public void setTarget(GameObject newTarget)
	{
		target = newTarget;
		found = false;
	}

	void computeDistance()
	{
		Vector3 v = (player != null) ? player.transform.position : new Vector3(0.0f, 0.0f, 0.0f);

		distance = Vector3.Distance (v, target.transform.position);

		if (distance == previousDistance)
			return;
		
		int nDistance = (int)distance;
		string str = "Distance Player-Target: " + nDistance.ToString();
		label.text = str;

		offset++;
		if (!timerObject.startMode && ((offset%100) == 0))
		{
			appLog ("Emiting in distance...");
			Dictionary<string,string> obj = new Dictionary<string,string> (); 
			obj["date"] = timerObject.GetTimestamp().ToString();
			obj["distance"] = nDistance.ToString();
			socket.Emit("distance", new JSONObject(obj));
		}

		giftImg.SetActive (true);

		if (distance < 1) {
			target.SetActive (false);
			found = true;
		}
	}

	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}

}

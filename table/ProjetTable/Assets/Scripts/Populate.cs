﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using SocketIO;
using System.Collections.Generic;

public class Populate : MonoBehaviour {

	private Button button;
	private SocketIOComponent socket;
	private Text logText;

	private int idObject = 0;

	private Object treeOliveTreeTransform;
	private Object treeBroadLeafTransform;
	private Object treeConiferTransform;
	private Object treePalmTreeTransform;

	private Object presentTransform;

	private List<GameObject> gameObjectInstances;
	private DistanceScript distanceObject;

	// Use this for initialization
	void Start () {
		gameObjectInstances = new List<GameObject>();

		// load Resources
		treeOliveTreeTransform = Resources.Load ("Tree/Tree");
		treeBroadLeafTransform = Resources.Load ("Tree/Broadleaf");
		treeConiferTransform = Resources.Load ("Tree/Conifer");
		treePalmTreeTransform = Resources.Load ("Tree/PalmTree");
		presentTransform = Resources.Load ("Present");

		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();
		button = GameObject.Find ("Populate").GetComponent<Button>();

		distanceObject = GameObject.Find ("Distance").GetComponent<DistanceScript>();
	
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		button.onClick.AddListener (OnSendClicked);
	}

	void OnSendClicked(){

		StartCoroutine ("SendResetRoutine");
		foreach (GameObject o in gameObjectInstances)  // Now disable them all in one go.
		{      
			Destroy(o);
		} 
		gameObjectInstances = new List<GameObject> ();

		int nbInstances = 100;

		for (int i = 0; i < nbInstances; i++) {
			StartCoroutine ("SendTreeRoutine");
		}

		StartCoroutine ("SendBuildingRoutine");
		StartCoroutine ("SendPresentRoutine");
	}

	public void PutNewPresent()
	{
		StartCoroutine ("SendPresentRoutine");
	}

	private IEnumerator SendResetRoutine()
	{
		Dictionary<string, string> reset = new Dictionary<string, string>();
		reset["action"] = "RESET";
		socket.Emit("object", new JSONObject(reset));

		yield return new WaitForSeconds(1);	
	}


	private IEnumerator SendTreeRoutine()
	{
		Object treeTransform;
		string name;

		Vector3 rotation = getRandomRotationY(180);

		// you hav to define here configuration for each tree 
		// example for olive tree: you have to rotate of 270° in x
		switch ((int) Random.Range(0, 2)) {
//		case 0:
//			treeTransform = treeOliveTreeTransform;
//			name = "olive_tree";
//			rotation.x = 270;
//			break;
		case 0:
			treeTransform = treeBroadLeafTransform;
			name = "broadleaf_tree";
			break;
		default:
			treeTransform = treeConiferTransform;
			name = "conifer";
			break;
//		default: 
//			treeTransform = treePalmTreeTransform;
//			name = "palm_tree";
//			break;
		}
			
		yield return SendRoutine("trees", name, treeTransform, getRandomPositionXZ(45.0f), rotation, false);
	}

	private IEnumerator SendPresentRoutine()
	{
		yield return SendRoutine("to_be_found", "present", presentTransform, getRandomPositionXZ(25.0f), new Vector3(0.0f, 0.0f, 0.0f), true);
	}

	public IEnumerator SendBuildingRoutine() {
		Object buildingTransform;
		string name;

		Vector3 rotation = getRandomRotationY(180);
		Vector3 position = getRandomPositionXZ (35.0f);

		// you have to define here configuration for each tree 
		// example for olive tree: you have to rotate of 270° in x
		switch ((int)Random.Range (0, 5)) {
		case 0:
			name = "Village_House_V1";
			buildingTransform = Resources.Load ("Village/" + name);
			position.y = -0.3f;
			break;
		case 1:
			name = "Village_House_V2";
			buildingTransform = Resources.Load ("Village/" + name);
			position.y = -0.29f;
			break;
		case 2:
			name = "Village_House_V3";
			buildingTransform = Resources.Load ("Village/" + name);
			position.y = -0.35f;
			break;
		case 3:
			name = "Village_House_V4";
			buildingTransform = Resources.Load ("Village/" + name);
			position.y = -0.4f;
			break;
		default:
			name = "Village_WindMill";
			buildingTransform = Resources.Load ("Village/" + name);
			position.y = -0.2f;
			break;
		}

		yield return SendRoutine ("building", name, buildingTransform, position, rotation, false);
	}

	/**
	 * Create a object in Table, and send it to the server.
	 * This object position and rotation are define by this method.
	 */
	private IEnumerator SendRoutine(string category, string name, Object objTransform, Vector3 position, Vector3 rotation, bool isTarget) {
		Dictionary<string, string> object1 = getObjectToSend();

		object1["type_category"] = category.ToLower();
		object1["type_name"] = name.ToLower();

		object1 ["action"] = "ADD";

		object1["position_x"] = position.x.ToString();
		object1["position_y"] = position.y.ToString();
		object1["position_z"] = position.z.ToString();
		object1["angle"] = rotation.y.ToString();

		socket.Emit("object", new JSONObject(object1));

		appLog ("Object sent: " + object1["type_name"]);

		// we create a new instance of objTransform
		GameObject instanceObject = (GameObject) Instantiate(objTransform);
		instanceObject.transform.position = position;

		// we manage only rotation around Y
		instanceObject.transform.rotation = Quaternion.Euler(rotation);

		// To select object and move it with mouse
		instanceObject.AddComponent<GizmosController>().id = object1["id_object"];

		if (isTarget) {
			distanceObject.setTarget(instanceObject);
			instanceObject.AddComponent<TargetScript>().target = instanceObject;
		}

		gameObjectInstances.Add (instanceObject);

		yield return new WaitForSeconds(1);		
	}


	public Dictionary<string, string> getObjectToSend(){
		Dictionary<string, string> dico = new Dictionary<string, string>();

		++idObject;
		dico["id_object"] = idObject.ToString();
		dico["type_category"] = "TODO";
		dico["type_name"] = "TODO";
		dico["position_x"] = "0.0";
		dico["position_y"] = "0.0";
		dico["position_z"] = "0.0";
		dico["angle"] = "0.0";
		dico["scale"] = "1.0";

		return dico;
	}


	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}
		
	public Vector3 getRandomPositionXZ(float range){
		float x = Random.Range(-range, range);
		float z = Random.Range(-range, range);
		return new Vector3(x, 0.0f, z);
	}

	/**
	 * by default, range can be 180
	 */
	public Vector3 getRandomRotationY(float range) {	
		return new Vector3 (0.0f, Random.Range (-range, range), 0.0f);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using SocketIO;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(MeshCollider))]

public class GizmosController : MonoBehaviour 
{
	private SocketIOComponent socket;
	private Text logText;

	private Vector3 screenPoint;
	private Vector3 offset;

	private Vector3 position;
	private Vector3 realPosition;

	public string id;

	void Start() 
	{
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();
	}

	void OnMouseDown()
	{
		if (Globals.mode == Mode.viewing)
			return; 
		
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
	}

	void OnMouseDrag()
	{
		if (Globals.mode == Mode.viewing)
			return; 
		
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

		Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		realPosition = new Vector3 (curPosition.x, 0, curPosition.z);
		transform.position = realPosition;
		position = curPosition;

		new WaitForSeconds(1);

		StartCoroutine ("UpdatePosition");
	}

	private IEnumerator UpdatePosition()
	{
		Dictionary<string, string> object1 = new Dictionary<string, string>();

		object1 ["id_object"] = id;
		object1 ["action"] = "UPDATE";
		object1["position_x"] = realPosition.x.ToString ();
		object1["position_y"] = realPosition.y.ToString();
		object1["position_z"] = realPosition.z.ToString();

		socket.Emit("object", new JSONObject(object1));

		appLog ("Position update for object " + id);

		yield return new WaitForSeconds(5);
	}

	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}

}
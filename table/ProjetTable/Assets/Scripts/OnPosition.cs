﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;

public class OnPosition : MonoBehaviour {
	
	private SocketIOComponent socket;
	private Text logText;

	private Dictionary<string, GameObject> playerObjectList;
	private DistanceScript distanceObject;


	void Start () {
		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();

		distanceObject = GameObject.Find ("Distance").GetComponent<DistanceScript>();

		playerObjectList = new Dictionary<string, GameObject>();

		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		socket.On("position", onPosition);
	}

	public void onPosition(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Position received: " + e.data);

		string action = e.data ["action"].ToString ().Replace ("\"", string.Empty);

		if (action.Equals ("UPDATE")) {
			PositionUpdate(e.data);
		} else if (action.Equals ("REMOVE")) {
			PositionRemove(e.data);
		}
	}

	public void PositionUpdate(JSONObject data)
	{
		string from = data["from"].ToString ().Replace ("\"", string.Empty);
		string xStr = data["x"].ToString ().Replace ("\"", string.Empty);
		string yStr = data["y"].ToString ().Replace ("\"", string.Empty);
		string zStr = data["z"].ToString ().Replace ("\"", string.Empty);

		float x, y, z;
		float.TryParse(xStr, out x);
		float.TryParse(yStr, out y);
		float.TryParse(zStr, out z);
		
		Vector3 vectPosition = new Vector3 (x, y, z);

		appLog("Player [" + getShortId(from) + "] is in [" + x + " ; " + y + " ; " + z + "]");

		if (playerObjectList.ContainsKey (from)) {
			// Update

			Debug.Log ("Updating playerObject for [" + getShortId(from) + "]");
			GameObject go;
			playerObjectList.TryGetValue (from, out go);

			go.transform.LookAt (vectPosition);
			go.transform.position = vectPosition;
		} else {
			// Create

			Debug.Log ("Creating playerObject for [" + getShortId(from) + "]");
			GameObject playerObject = (GameObject) Instantiate(Resources.Load("Teddybear"));
			playerObject.transform.position = vectPosition;

			playerObjectList.Add (from, playerObject);

			distanceObject.player = playerObject;
		}
	}

	public void PositionRemove(JSONObject data)
	{
		string from = data ["from"].ToString ().Replace ("\"", string.Empty);

		if (playerObjectList.ContainsKey(from))
		{
			appLog("Player [" + getShortId(from) + "] is now disconnected");

			//coordinatesList.Remove (from);

			GameObject go;
			playerObjectList.TryGetValue (from, out go);
			Destroy(go);

			playerObjectList.Remove (from);
		}
	}

	public void showPlayersPosition()
	{
	//	foreach(KeyValuePair<string, Vector3> entry in coordinatesList)
	//	{
	//		Debug.Log("Player " + getShortId(entry.Key) + " is in " + entry.Value.ToString());
	//	}
	}

	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}

	public string getShortId(string from){
		return from.Substring (0,5);
	}
}

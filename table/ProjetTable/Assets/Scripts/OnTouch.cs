﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class OnTouch : MonoBehaviour {

	public float speed = 0.1F;
	private Text logText;

	void Start(){
		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();
	}

	void Update() {
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
			// Get movement of the finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

			// Move object across XY plane
			transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0);

			appLog ("[OnTouch.cs] Touch detected");
		}
	}


	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScript : MonoBehaviour {

	public GameObject target;
	private DistanceScript distanceObject;
	public float distance;

	// Use this for initialization
	void Start () {
		distanceObject = GameObject.Find ("Distance").GetComponent<DistanceScript>();
	}
	
	// Update is called once per frame
	void Update () {
		if (distanceObject.player != null)
			HideOnContact ();
	}

	void HideOnContact()
	{
		distance = Vector3.Distance (distanceObject.player.transform.position, target.transform.position);
		if (distance < 1) {
			target.SetActive (false);
		}
	}
}

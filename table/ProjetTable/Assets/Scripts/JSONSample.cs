﻿using System;
using UnityEngine;

[Serializable]
public class Scale {
	public string value;
}

[Serializable]
public class Angle {
	public string ry;
}

[Serializable]
public class Position {
	public string x;
	public string y;
	public string z;
}

[Serializable]
public class Type {
	public string category;
	public string name;
}

[Serializable]
public class Object3D {
	public string action;
	public int id_object;
	public Type type;
	public Position pos;
	public Angle angle;
	public Scale scale;
}

[Serializable]
public class Map {
	public string _id;
	public string img;
	public string name;
	public Object3D[] objects;
}

[Serializable]
public class Stat {
	public int date;
	public int time;
	public string distance;
	public string direction;
}

public class JSONTest {
	void bleh(){
		Map map = new Map();
		JsonUtility.ToJson (map);
	}
//	public static void run(){
//		string json_string = "{\"_id\":\"5878e4fba73bb945fad3537f\",\"img\":\"path/to/foret/img\",\"name\":\"foret\",\"objects\":[{\"id_object\":1,\"type\":{\"category\":\"arbre\",\"name\":\"epicea\"},\"pos\":{\"x\":0,\"y\":0,\"z\":0},\"angle\":{\"ry\":0},\"scale\":{\"value\":1}},[{\"id_object\":2,\"type\":{\"category\":\"arbre\",\"name\":\"sapin\"},\"pos\":{\"x\":0,\"y\":0,\"z\":0},\"angle\":{\"ry\":0},\"scale\":{\"value\":1}}]]}";
//		Map json = JsonUtility.FromJson<Map> (json_string);
//		Debug.Log (json.name);
//		Debug.Log (json.objects[0].type.name);
//	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals {
	// application mode
	public static Mode mode = Mode.creating;
	// application currentTouch
	public static int currTouch = 0;
}

public enum Mode {
	viewing, // mode when viewing map (changing camera view)
	creating, // mode when creating map
	interacting // mode when interacting with players by the map
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using UnityEngine.UI;
using System;

public class TUIOScript : MonoBehaviour {
	
	private float timeLimit = 0.1f;

	private SocketIOComponent socket;
	private Text logText;

	public GameObject canvas;

	public GameObject populateObject;
	public GameObject presentObject;
	public GameObject hailObject;
	public GameObject timerObject;
	public GameObject cameraPanelObject;

	private Dictionary<string, List<GameObject>> bindingMap;

	// Use this for initialization
	void Start () {
		socket = GameObject.Find("TUIO").GetComponent<SocketIOComponent>();
		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();

		socket.On("connect", onConnect);
		socket.On("CREATE", onCreate);
		socket.On("UPDATE", onCreate);
		socket.On("DELETE", onDelete);

		bindingMap = new Dictionary<string, List<GameObject>>();

		List<GameObject> constructBtns = new List<GameObject> ();
		constructBtns.Add (populateObject);
		constructBtns.Add (presentObject);
		constructBtns.Add (hailObject);
		bindingMap ["4"] = constructBtns;

		List<GameObject> timerBtns = new List<GameObject> ();
		timerBtns.Add (timerObject);
		bindingMap ["A8"] = timerBtns;

		List<GameObject> cameraPanel = new List<GameObject> ();
		cameraPanel.Add (cameraPanelObject);
		bindingMap ["40"] = cameraPanel;

		setAllDisplay (false);

//		setDisplay ("4", true, 200.0f, -100.0f);
//		setDisplay ("A8", true, 400.0f, -100.0f);
//		setDisplay ("40", true, 0.0f, -100.0f);

		appLog ("Resolution: " + Screen.width + "x" + Screen.height);
	}

	public void onConnect(SocketIOEvent e)
	{
		appLog ("Connected to TUIO");
	}

	public void onDelete(SocketIOEvent e)
	{
		string sType = e.data ["type"].ToString ().Replace ("\"", string.Empty);

		if (!(sType.Equals ("TAG")))
			return;

		//appLog ("TUIO TAG DELETE signal received: " + e.data);

		setDisplay (e.data["tagId"].ToString().Replace ("\"", string.Empty), false, 0.0f, 0.0f);
	}

	public void onCreate(SocketIOEvent e)
	{
		if (timeLimit >= 0) {
			timeLimit -= Time.deltaTime;
			return;
		} else {
			timeLimit = 0.1f;
		}

		string sType = e.data ["type"].ToString ().Replace ("\"", string.Empty);

		if (!(sType.Equals ("TAG")))
			return;

		//appLog ("TUIO TAG CREATE signal received: " + e.data);

		string xStr = e.data ["x"].ToString ().Replace ("\"", string.Empty);
		string yStr = e.data ["y"].ToString ().Replace ("\"", string.Empty);

		float x;
		float y;
		float.TryParse (xStr, out x);
		float.TryParse (yStr, out y);

		setDisplay (e.data["tagId"].ToString().Replace ("\"", string.Empty), true, x * (float) Screen.width, -1 * y * (float) Screen.height);
	}

	void setDisplay(string id, bool show, float x, float y)
	{
		//appLog ("Set Display - id: " + id + " - bool: " + show + " - x:" + x.ToString() + " - y:" + y.ToString());

		List<GameObject> buttonSet = bindingMap[id];

		float count = 0.0f;
		float scale = 35.0f; 
		foreach (GameObject btn in buttonSet) {
			btn.SetActive (show);

			if (show) {
				btn.GetComponent<RectTransform> ().anchoredPosition = new Vector2(x + 95.0f, y - (count * scale) - 60.0f);
				count++;
			}
		}
	}

	void setAllDisplay(bool show)
	{
		foreach (KeyValuePair<string,List<GameObject>> kvp in bindingMap)
		{
			setDisplay (kvp.Key, show, 0, 0);
		}
	}

	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}
}

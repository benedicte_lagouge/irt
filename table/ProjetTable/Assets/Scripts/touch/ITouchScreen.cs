﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITouchScreen {
	// Touches screen
	void OnTouchBegan();
	void OnTouchEnded();
	void OnTouchMoved();
	void OnTouchStayed();
}

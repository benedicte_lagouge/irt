﻿using UnityEngine;
using System.Collections;

public class TouchObjectEventManager : MonoBehaviour 
{
	private int touch2Watch = int.MaxValue; // because none has 2147483647 fingers at once on a screen
	private Ray ray;//this will be the ray that we cast from our touch into the scene
	private RaycastHit rayHitInfo = new RaycastHit();//return the info of the object that was hit by the ray
	private ITouchObject touchedObject = null;

	public virtual void Update () 
	{
		if(Globals.mode == Mode.interacting) // only in interacting mode
		foreach(Touch touch in Input.touches)
		{
			Globals.currTouch = touch.fingerId;
			ray = Camera.main.ScreenPointToRay(touch.position);//creates ray from screen point position
			//if the ray hit something, only if it hit something
			if(Physics.Raycast(ray, out rayHitInfo))
			{
				//if the thing that was hit implements ITouchable3D
				touchedObject = rayHitInfo.transform.GetComponent(typeof(ITouchObject)) as ITouchObject;
				//Debug.Log (touchedObject);
				//If the ray hit something and it implements ITouchable3D
				if (touchedObject != null)
				{
					switch(touch.phase)
					{
					case TouchPhase.Began:
						touchedObject.OnTouchBegan();
						touch2Watch = Globals.currTouch;
						break;
					case TouchPhase.Ended:
						touchedObject.OnTouchEnded();
						break;
					case TouchPhase.Moved:
						touchedObject.OnTouchMoved();
						break;
					case TouchPhase.Stationary:
						touchedObject.OnTouchStayed();
						break;
					}
				}
			}
			// On Anywhere
			else if (touchedObject != null && touch2Watch == Globals.currTouch)
			{
				switch(touch.phase)	{
				case TouchPhase.Ended:
					touchedObject.OnTouchEndedAnywhere();
					touchedObject = null;
					break;
				case TouchPhase.Moved:
					touchedObject.OnTouchMovedAnywhere();
					break;
				case TouchPhase.Stationary:
					touchedObject.OnTouchStayedAnywhere();
					break;
				}
			}
		}
	}
}
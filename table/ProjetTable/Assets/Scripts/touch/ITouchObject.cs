﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITouchObject {
	// Touches on object
	void OnTouchBegan();
	void OnTouchEnded();
	void OnTouchMoved();
	void OnTouchStayed();
	// Touches elsewhere
	void OnTouchEndedAnywhere();
	void OnTouchMovedAnywhere();
	void OnTouchStayedAnywhere();
}

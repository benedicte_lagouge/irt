﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenMoves : ITouchScreen  {
	private float limitZoomIn = 1.0f;
	private float limitZoomOut = 200.0f;

	public float zoomSpeed = 13.0f;
	// buckets for caching our touch positions
	private Vector2 currTouch1 = Vector2.zero,
	lastTouch1 = Vector2.zero,
	currTouch2 = Vector2.zero,
	lastTouch2 = Vector2.zero;
	// used for holding our distances and calculating our zoomFactor
	private float currDist = 0.0f,
	lastDist = 0.0f,
	zoomFactor = 0.0f;

	#region ITouchScreen implementation
	public void OnTouchBegan (){}

	public void OnTouchEnded (){}

	public void OnTouchMoved (){
		// Zoom detection
		DetectZoomTouch();
		if (IsZooming ()) // Currently zooming (only do this)
			Zoom ();
		else
			Translate ();
	}

	public void OnTouchStayed (){
		// We keep zooming when touch stayed because we still want 
		// to zoom if one finger doesn't move but the second does
		// Zoom detection
		DetectZoomTouch();
		if (IsZooming ()) // Currently zooming (only do this)
			Zoom ();
	}
	#endregion

	private bool IsZooming(){
		return (Globals.currTouch >= 1);
	}

	private void DetectZoomTouch(){
		// Caches touch positions for each finger
		switch(Globals.currTouch) {
		case 0:// first touch
			currTouch1 = Input.GetTouch(0).position;
			lastTouch1 = currTouch1 - Input.GetTouch(0).deltaPosition;
			break;
		case 1:// second touch
			currTouch2 = Input.GetTouch(1).position;
			lastTouch2 = currTouch2 - Input.GetTouch(1).deltaPosition;
			break;
		}
	}

	//find distance between the 2 touches 1 frame before & current frame
	private void Zoom() {
		// finds the distance between two touches
		currDist = Vector2.Distance(currTouch1, currTouch2);
		lastDist = Vector2.Distance(lastTouch1, lastTouch2);
		// Calculate the zoom magnitude
		// Zoom in or out depending on this magnitude (i.e. lastDist - currDist) (+ = increasing, - = decreasing)
		zoomFactor = -Mathf.Clamp(lastDist - currDist, -30.0f, 30.0f);
		// apply zoom to our camera
		Vector3 newPosition = Vector3.forward * zoomFactor * zoomSpeed * Time.deltaTime;
//		if((newPosition.y > limitZoomIn) && (newPosition.y < limitZoomOut))
		Camera.main.transform.Translate(newPosition);
	}

	private void Translate(){
		Camera.main.transform.Translate (new Vector3 (-Input.GetTouch (0).deltaPosition.x, 
											-Input.GetTouch (0).deltaPosition.y, 0.0f) * 
										zoomSpeed * Time.deltaTime);
	}
}
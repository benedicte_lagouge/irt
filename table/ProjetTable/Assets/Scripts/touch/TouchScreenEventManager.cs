﻿using UnityEngine;
using System.Collections;

public class TouchScreenEventManager : MonoBehaviour 
{
	private int touch2Watch = 1024; // because none has 1024 fingers at once on a screen
	public ITouchScreen screen;

	public virtual void Update () 
	{
		if(Globals.mode == Mode.viewing) // only in viewing mode
		foreach(Touch touch in Input.touches)
		{
			Globals.currTouch = touch.fingerId;
			switch(touch.phase)
			{
			case TouchPhase.Began:
				screen.OnTouchBegan();
				touch2Watch = Globals.currTouch;
				break;
			case TouchPhase.Ended:
				screen.OnTouchEnded();
				break;
			case TouchPhase.Moved:
				screen.OnTouchMoved();
				break;
			case TouchPhase.Stationary:
				screen.OnTouchStayed();
				break;
			}
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnPutPresent : MonoBehaviour {

	Populate populateObject;
	private Button button;

	// Use this for initialization
	void Start () {
		populateObject = GameObject.Find ("Populate").GetComponent<Populate>();
		button = GameObject.Find ("Send Present").GetComponent<Button>();
		button.onClick.AddListener (OnClick);
	}

	void OnClick(){
		populateObject.PutNewPresent ();
	}
}

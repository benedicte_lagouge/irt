﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;
using System;

public class TimerScript : MonoBehaviour {

	private SocketIOComponent socket;

	public GameObject timerImg;

	private Button button;
	private Text label;

	public DistanceScript distanceObject;

	private int duration;
	private int startedAt;
	private double distance;

	private string direction;

	public bool startMode;

	private Text logText;

	// Use this for initialization
	void Start () {
		timerImg.SetActive(false);

		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		distanceObject = GameObject.Find ("Distance").GetComponent<DistanceScript>();
		label = GameObject.Find ("Time Text").GetComponent<UnityEngine.UI.Text>();
		button = GameObject.Find ("Start Timer").GetComponent<UnityEngine.UI.Button>();
		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();

		// Set button listener
		button.onClick.AddListener (StartTimer);

		startMode = true;
		startedAt = -1;
	}
	
	// Update is called once per frame
	void Update () {
		if (startedAt > 0) {
			int tempTime = GetTimestamp() - startedAt;
			label.text = "Time: " + tempTime.ToString () + "s";
			timerImg.SetActive(true);
		}

		if (distanceObject.target != null)
		{
			if (distanceObject.found) {
				StopTimer ();
				distanceObject.target = null;
			}
		}
	}

	void StartTimer ()
	{
		if (distanceObject.player == null || distanceObject.target == null || !startMode) {
			return;
		}

		direction = (distanceObject.target.transform.position.x <= 0) ? "left": "right";

		// Save timer
		startedAt = GetTimestamp();

		// Save distance
		distance = distanceObject.distance;

		// Set button text
		button.GetComponentInChildren<Text>().text = "Stop Timer";

		startMode = false;
		button.onClick.AddListener (StopTimer);

		appLog ("Timer started -> distance: " + distance.ToString() + " - direction: " + direction);
	}

	void StopTimer()
	{
		if (startMode)
			return;

		// Compute diff
		int tmp = GetTimestamp();
		duration = tmp - startedAt;
		Debug.Log ("duration : " + duration);

		startedAt = -1;

		// Start routine
		StartCoroutine ("SendStatRoutine");

		// Set button text
		button.GetComponentInChildren<Text>().text = "Start Timer";

		startMode = true;
		button.onClick.AddListener (StartTimer);

		appLog ("Timer stoped with -> distance: " + distance.ToString() + " - direction: " + direction + " - time: " + duration.ToString()+"s");
	}

	private IEnumerator SendStatRoutine() {
		Dictionary<string, string> objectToSend = new Dictionary<string, string>();

		objectToSend ["date"] = GetTimestamp().ToString();
		objectToSend ["time"] = duration.ToString();
		objectToSend ["distance"] = distance.ToString();
		objectToSend ["direction"] = direction;

		socket.Emit("stats", new JSONObject(objectToSend));

		yield return new WaitForSeconds(1);		
	}

	public int GetTimestamp()
	{
		DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		int currentEpochTime = (int)(DateTime.UtcNow - epochStart).TotalSeconds;
		return currentEpochTime;
	}

	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraController : TouchScreenEventManager {

	private Text logText;

	private Text text;
	private Button button;

	private Color myGrey;

	// Use this for initialization
	void Start () {
		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();

		text = GameObject.Find ("Finger Text").GetComponent<Text>();
		button = GameObject.Find ("Finger Camera").GetComponent<Button>();
		button.onClick.AddListener (OnClick);

		myGrey = new Color ();
		ColorUtility.TryParseHtmlString ("#323232FF", out myGrey);

		base.screen = new ScreenMoves ();
	}

	void Update() {
		base.Update ();
		// do things if needed
	}

	void OnClick() {
		Globals.mode = (Globals.mode == Mode.creating) ? Mode.viewing : Mode.creating; 
		button.GetComponent<Image>().color = (Globals.mode == Mode.creating) ? Color.white : myGrey;
		text.GetComponent<Text>().color = (Globals.mode == Mode.creating) ? myGrey : Color.white;
	}

	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}
}

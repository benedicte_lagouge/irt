﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using SocketIO;
using System.Collections.Generic;

public class OnPutHail : MonoBehaviour {

	private Button button;
	private SocketIOComponent socket;
	private Text logText;

	private Object hailTransform;

	private bool exists = false;

	// Use this for initialization
	void Start () {
		hailTransform = Resources.Load ("Hail");

		logText = GameObject.Find ("App Logs").GetComponent<UnityEngine.UI.Text>();
		button = GameObject.Find ("Send Hail").GetComponent<Button>();

		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		button.onClick.AddListener (onSendHailClicked);

	}

	void onSendHailClicked(){
		if (!exists)
		{
			StartCoroutine ("SendHailRoutine");
			exists = true;
		}
	}

	private IEnumerator SendHailRoutine()
	{
		Dictionary<string, string> object1 = getObjectToSend();
		Vector3 pos = getHailPosition ();

		object1["type_category"] = "ambiance";
		object1["type_name"] = "hail";

		object1["action"] = "ADD";

		object1["position_x"] = pos.x.ToString();
		object1["position_y"] = pos.y.ToString();
		object1["position_z"] = pos.z.ToString();

		socket.Emit("object",new JSONObject(object1));

		appLog ("Object sent: " + object1["type_name"]);

		GameObject instanceObject = (GameObject) Instantiate(hailTransform);
		instanceObject.transform.position = pos;

		yield return new WaitForSeconds(1);
	}

	public void appLog(string msg)
	{
		logText.text = msg + '\n' + logText.text;
	}

	public Dictionary<string, string> getObjectToSend(){

		Dictionary<string, string> dico = new Dictionary<string, string>();

		dico["id_object"] = "0";
		dico["type_category"] = "TODO";
		dico["type_name"] = "TODO";
		dico["position_x"] = "0.0";
		dico["position_y"] = "0.0";
		dico["position_z"] = "0.0";
		dico["angle"] = "0.0";
		dico["scale"] = "1.0";

		return dico;
	}

	public Vector3 getHailPosition(){
		return new Vector3(0.0f, 20.0f, 0.0f);
	}
}

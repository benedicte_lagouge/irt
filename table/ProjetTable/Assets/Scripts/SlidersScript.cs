﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // Required when Using UI elements.

public class SlidersScript : MonoBehaviour 
{
	public GameObject mainCamera;

	private Slider zoomSlider;
	private Slider xSlider;
	private Slider zSlider;
	private Slider angleSlider;

	public void Start()
	{
		zoomSlider = GameObject.Find ("SliderZoom").GetComponent<UnityEngine.UI.Slider> ();
		xSlider = GameObject.Find ("SliderX").GetComponent<UnityEngine.UI.Slider> ();
		zSlider = GameObject.Find ("SliderZ").GetComponent<UnityEngine.UI.Slider> ();
		angleSlider = GameObject.Find ("SliderAngle").GetComponent<UnityEngine.UI.Slider> ();

		zoomSlider.onValueChanged.AddListener (delegate {SetCamPos ();});
		xSlider.onValueChanged.AddListener (delegate {SetCamPos ();});
		zSlider.onValueChanged.AddListener (delegate {SetCamPos ();});
		angleSlider.onValueChanged.AddListener (delegate {SetAngle ();});
	}

	public void Update()
	{
		Vector3 pos;
		if(Input.GetAxis("Mouse ScrollWheel") < 0)
		{
			float previousY = Camera.main.transform.position.y;
			float y = previousY + ((previousY >= zoomSlider.maxValue) ? 0.0f:1.0f);
			zoomSlider.value = y;
			SetCamPos ();
		}

		if(Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			float previousY = Camera.main.transform.position.y;
			float y = previousY - ((previousY <= zoomSlider.minValue)? 0.0f:1.0f);
			zoomSlider.value = y;
			SetCamPos ();
		}
	}

	// Invoked when the value of the slider changes.
	public void SetCamPos()
	{
		Vector3 v = new Vector3 (xSlider.value, zoomSlider.value, zSlider.value);
		mainCamera.transform.position = v;
	}

	public void SetAngle()
	{
		mainCamera.transform.rotation = Quaternion.Euler(angleSlider.value, 0.0f, 0.0f);
	}
}
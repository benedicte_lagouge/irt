﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private float speed;
	public float maxSpeed;
	public float movePosition;

	private Rigidbody rb;

	// method called at first occurence of the object in the frame
	void Start () {
		rb = GetComponent<Rigidbody> ();
		speed = 1;
		foreach (string i in Input.GetJoystickNames()) 
			Debug.Log(i);
		JSONTest.run ();
	}

	// method called before physics calculations
	void FixedUpdate () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		float moveHorizontalCam = Input.GetAxis ("Horizontal_cam");
		float moveVerticalCam = Input.GetAxis ("Vertical_cam");
		if (moveHorizontal > 0)
			moveHorizontal *= 1 / 0.74f;

		// Debug.Log ("x : " + moveHorizontal + " | z : " + moveVertical);
		// Debug.Log ("x_cam : " + moveHorizontalCam + " | z_cam : " + moveVerticalCam);
		// Debug.Log ("rot_y : " + transform.localEulerAngles.y);
		Vector3 movement = Quaternion.Euler(0.0f, transform.localEulerAngles.y, 0.0f) * new Vector3 (moveVertical, 0.0f, moveHorizontal);
		transform.rotation *= Quaternion.Euler(0.0f, moveHorizontalCam * 5, 0.0f);

		// Debug.Log ("rot_x : " + movement.x + " | rot_z : " + movement.z);

		if ((moveHorizontal == 0) && (moveVertical == 0)) { // no move
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;
			speed = 1;
		}

		//Debug.Log ("speed : " + speed);
		if(speed < maxSpeed) // max speed
			speed *= (1 + movement.magnitude / 100);

		if (movePosition != 0)
			rb.MovePosition (transform.position + movement * speed * movePosition);
	}

}

﻿using System;
using UnityEngine;

[Serializable]
public class Scale {
	public float value;
}

[Serializable]
public class Angle {
	public float ry;
}

[Serializable]
public class Position {
	public float x;
	public float y;
	public float z;
}

[Serializable]
public class Type {
	public string category;
	public string name;
}

[Serializable]
public class Object {
	public int id_object;
	public Type type;
	public Position pos;
	public Angle angle;
	public Scale scale;
}

[Serializable]
public class Map {
	public string _id;
	public string img;
	public string name;
	public Object[] objects;
}

public class JSONTest {
	
	public static void run(){
		string json_string = "{\"_id\":\"5878e4fba73bb945fad3537f\",\"img\":\"path/to/foret/img\",\"name\":\"foret\",\"objects\":[{\"id_object\":1,\"type\":{\"category\":\"arbre\",\"name\":\"epicea\"},\"pos\":{\"x\":0,\"y\":0,\"z\":0},\"angle\":{\"ry\":0},\"scale\":{\"value\":1}},[{\"id_object\":2,\"type\":{\"category\":\"arbre\",\"name\":\"sapin\"},\"pos\":{\"x\":0,\"y\":0,\"z\":0},\"angle\":{\"ry\":0},\"scale\":{\"value\":1}}]]}";
		Map json = JsonUtility.FromJson<Map> (json_string);
		Debug.Log (json.name);
		Debug.Log (json.objects[0].type.name);
	}
}
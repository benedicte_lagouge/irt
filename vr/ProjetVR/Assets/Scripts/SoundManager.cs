﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager{

	private AudioSource walk_sound;

	public SoundManager() {
		GameObject cameraRoot = (GameObject)GameObject.Find ("CameraRoot");
		walk_sound = cameraRoot.GetComponent<AudioSource> ();
	}

	public void PlayWalk()
	{        
		if (walk_sound != null && !walk_sound.isPlaying)
		{
			Debug.Log ("PLAY");
			walk_sound.volume = Random.Range(0.8f, 1);
			walk_sound.pitch = Random.Range(0.8f, 1.1f);
			walk_sound.Play();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
//ça sert à rien comme moi
public class Connexions : MonoBehaviour
{
	private SocketIOComponent socket;

	public void Start() 
	{
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		socket.On("connect", Connecting);
		socket.On("res", Res);
		
		StartCoroutine("Blou");
	}

	
	private IEnumerator Blou()
	{
		// wait 1 seconds and continue
		yield return new WaitForSeconds(1);
		SendMove(1,2,3,4);
		yield return new WaitForSeconds(1);
		HoldingObject(1,2);
		yield return new WaitForSeconds(1);
	}

/* Connection to the server*/	
	public void Connecting(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Connect received: " + e.name + " " + e.data);
	}


/* Sending new position of user. */
//TODO use the properties of the object we assigned this script for getting automatically the parameters.
	public void SendMove(int id_user, float x, float y, float z)
	{
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["id_user"] = id_user.ToString();
		data["x"] = x.ToString();
		data["y"] = y.ToString();
		data["z"] = z.ToString();
		socket.Emit("move",new JSONObject(data));
	}

/* Notify the server that we are holding an object */
	public void HoldingObject(int id_user, int id_object)
	{
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["id_user"] = id_user.ToString();
		data["id_object"] = id_object.ToString();
		socket.Emit("holdObject",new JSONObject(data));
	}


/*
sendMap([Object1; Object2; (...)])
sendObject(Object)
*/

/*Getting a response of server */

	public void Res(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Res received: " + e.name + " " + e.data);

		if (e.data == null) 
		{
			Debug.Log("BLOUBLOUBLOU"); 
			return; 
		}
	}
}

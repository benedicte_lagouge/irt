﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ObjectInformations {
	public int id_object;
	public string type_category;
	public string type_name;
	public int position_x;
	public int position_y;
	public int position_z;
	public int angle;
	public int scale;
	public string action;
}

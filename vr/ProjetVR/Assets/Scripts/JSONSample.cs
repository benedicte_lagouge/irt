﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Scale {
	public string value;
}

[Serializable]
public class Angle {
	public string ry;
}

[Serializable]
public class Position {
	public string x;
	public string y;
	public string z;
}

[Serializable]
public class Type {
	public string category;
	public string name;
	public static Dictionary<string,Dictionary<string, GameObject>> map;

	public Type(string category,string name) {
		this.category = category;
		this.name = name;
	}

	static Type() {
		map = new Dictionary<string,Dictionary<string, GameObject>>();
		Dictionary<string, GameObject> christmas = new Dictionary<string, GameObject>();
		christmas.Add("present", (GameObject) Resources.Load("Present"));
		Dictionary<string, GameObject> ambiance = new Dictionary<string, GameObject>();
		ambiance.Add("hail", (GameObject) Resources.Load("Hail"));
		Dictionary<string, GameObject> trees = new Dictionary<string, GameObject>();
		trees.Add ("broadleaf_tree", (GameObject) Resources.Load("Tree/Broadleaf"));
		trees.Add ("conifer", (GameObject) Resources.Load("Tree/Conifer"));
		Dictionary<string, GameObject> build = new Dictionary<string, GameObject>();
		build.Add ("Village_House_V1", (GameObject) Resources.Load ("Village/Village_House_V1"));
		build.Add ("Village_House_V2", (GameObject) Resources.Load ("Village/Village_House_V2"));
		build.Add ("Village_House_V3", (GameObject) Resources.Load ("Village/Village_House_V3"));
		build.Add ("Village_House_V4", (GameObject) Resources.Load ("Village/Village_House_V4"));
		build.Add ("Village_WindMill", (GameObject) Resources.Load ("Village/Village_WindMill"));

		map.Add ("christmas", christmas);
		map.Add ("ambiance", ambiance);
		map.Add ("trees", trees);
		map.Add ("build", build);
	}
}

[Serializable]
public class Object3D {
	public int id_object;
	public Type type;
	public Position pos;
	public Angle angle;
	public Scale scale;
	public string action;
}

[Serializable]
public class Map {
	public string _id;
	public string img;
	public string name;
	public Object3D[] objects;
}
	
public class JSONTest {
	
	public static void run(){
		string json_string = "{\"_id\":\"5878e4fba73bb945fad3537f\",\"img\":\"path/to/foret/img\",\"name\":\"foret\",\"objects\":[{\"id_object\":1,\"type\":{\"category\":\"arbre\",\"name\":\"epicea\"},\"pos\":{\"x\":0,\"y\":0,\"z\":0},\"angle\":{\"ry\":0},\"scale\":{\"value\":1}},[{\"id_object\":2,\"type\":{\"category\":\"arbre\",\"name\":\"sapin\"},\"pos\":{\"x\":0,\"y\":0,\"z\":0},\"angle\":{\"ry\":0},\"scale\":{\"value\":1}}]]}";
		Map json = JsonUtility.FromJson<Map> (json_string);
		Debug.Log (json.name);
		Debug.Log (json.objects[0].type.name);
	}
}
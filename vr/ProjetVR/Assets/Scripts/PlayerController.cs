﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System.Globalization;

public class PlayerController : MonoBehaviour {

	private SocketIOComponent socket;
	private float positionScaleFactor;

	public TextMesh debug;
	public GameObject mainCamera;

	private float speed;
	private Vector3 movement;

	private Vector3 last_pos;

	public float maxSpeed;
	public float speedFactor;

	private Rigidbody rb;

	private SoundManager soundManager;

	// method called at first occurence of the object in the frame
	void Start () {
		soundManager = new SoundManager();
		// Object getting
		rb = GetComponent<Rigidbody> ();
		mainCamera = GameObject.Find ("Main Camera");
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		// Socket connection
		socket.On("connect", Connecting);

		// Speed init
		speed = 1;
		positionScaleFactor = 0.03f;

		// Displaying joysticks
		foreach (string i in Input.GetJoystickNames()) 
			Debug.Log(i);
	}

	// method called before physics calculations
	void FixedUpdate () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		//debug.text = "x : " + moveHorizontal + " | z : " + moveVertical;

		movement.x = moveVertical;
		movement.z = moveHorizontal;
		movement = Quaternion.Euler (0.0f, mainCamera.transform.localEulerAngles.y, 0.0f) * movement;
		transform.rotation = Quaternion.Euler (0.0f, mainCamera.transform.localEulerAngles.y, 0.0f);

		if ((moveHorizontal == 0) && (moveVertical == 0)) { // no move
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;
			speed = 1;
		}

		if(speed < maxSpeed) // max speed
			speed *= (1 + movement.magnitude / 100);

		rb.MovePosition (transform.position + movement * speed * positionScaleFactor * speedFactor);


		// Update position only if moved
		if (((last_pos.x - transform.position.x) > 0.1) || ((last_pos.x - transform.position.x) < -0.1) ||
		    ((last_pos.y - transform.position.y) > 0.1) || ((last_pos.y - transform.position.y) < -0.1) ||
		    ((last_pos.z - transform.position.z) > 0.1) || ((last_pos.z - transform.position.z) < -0.1)) {
			last_pos = transform.position;
			StartCoroutine ("Moving");
			soundManager.PlayWalk ();
		}
	}

	public void Connecting(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Connect received: " + e.name + " " + e.data);
	}

	private IEnumerator Moving() 
	{
		Vector3 vector = transform.position;
		Dictionary<string, string> data_vector = new Dictionary<string, string> ();
		data_vector ["x"] = vector.x.ToString("F4",new CultureInfo("en-US"));
		data_vector ["y"] = vector.y.ToString("F4",new CultureInfo("en-US"));
		data_vector ["z"] = vector.z.ToString("F4",new CultureInfo("en-US"));
		data_vector ["action"] = "UPDATE";
		debug.text = ("BLOU " + vector.x.ToString () + " " + data_vector ["x"] + " " + data_vector ["y"] + " " + data_vector ["z"]);
		socket.Emit ("position", new JSONObject (data_vector));

		yield return new WaitForSeconds(1);
	}
}

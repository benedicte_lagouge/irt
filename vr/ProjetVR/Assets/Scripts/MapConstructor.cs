﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;

public class MapConstructor : MonoBehaviour {

	private SocketIOComponent socket;
	public UnityEngine.Object hail;

	public UnityEngine.Object olive_tree;
	public UnityEngine.Object broadleaf_tree;
	public UnityEngine.Object conifer_tree;

	public UnityEngine.Object present;

	public UnityEngine.Object village_house_v1;
	public UnityEngine.Object village_house_v2;
	public UnityEngine.Object village_house_v3;
	public UnityEngine.Object village_house_v4;
	public UnityEngine.Object village_windmill;

	private Dictionary<string, GameObject> gameObjects;
	private List<GameObject> objectsToBeFound;

	// Use this for initialization
	public void Start () 
	{
		gameObjects = new Dictionary<string, GameObject> ();
		objectsToBeFound = new List<GameObject> ();

		hail = Resources.Load ("Hail");

		olive_tree = Resources.Load ("Tree/Tree");
		broadleaf_tree = Resources.Load ("Tree/Broadleaf");
		conifer_tree = Resources.Load ("Tree/Conifer");

		present = Resources.Load ("Present");

		village_house_v1 = Resources.Load ("Village/Village_House_V1");
		village_house_v2 = Resources.Load ("Village/Village_House_V2");
		village_house_v3 = Resources.Load ("Village/Village_House_V3");
		village_house_v4 = Resources.Load ("Village/Village_House_V4");

		village_windmill = Resources.Load ("Village/Village_WindMill");

		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		socket.On("connect", Connecting);
		socket.On("object",GetObject);

		StartCoroutine ("Register");
	}
	
	// Update is called once per frame
	public void Update () 
	{
		Debug.Log ("UPDATE");
		if (objectsToBeFound.Count == 0) {
			//TODO lancer un son? envoyer un message au serveur?
			Debug.Log("VIIIIIIIIIIIIIIDE");
		} 
		else {
			foreach (GameObject gameObject in objectsToBeFound) {
				Vector3 vector = GameObject.Find ("CameraRoot").transform.position;
				Vector3 vector_present = gameObject.transform.position;

				//check if object is found
				if (Vector3.Distance(vector,vector_present)<1) {
					objectsToBeFound.Remove (gameObject);
					Destroy (gameObject);
					//TODO send info to server?
				}
			}
		}
	}

	//Register the vr and send the initial position of the player( basically (0,0,0) ) 
	private IEnumerator Register()
	{
		// wait 1 seconds and continue
		yield return new WaitForSeconds(1);

		Dictionary<string, string> data = new Dictionary<string, string>();
		data["type"] = "vr";
		socket.Emit("register",new JSONObject(data));

		yield return new WaitForSeconds(1);

		Vector3 vector = GameObject.Find ("CameraRoot").transform.position;
		Dictionary<string, string> data_vector = new Dictionary<string, string>();
		data_vector ["x"] = vector.x.ToString("F2");
		data_vector ["y"] = vector.y.ToString("F2");
		data_vector ["z"] = vector.z.ToString("F2");
		data_vector ["action"] = "UPDATE";
		Debug.Log ("BLOU " + vector.x.ToString () + " " + data_vector ["x"] + " " + data_vector ["y"] + " " + data_vector ["z"]);
		socket.Emit ("position", new JSONObject (data_vector));

		yield return new WaitForSeconds(1);
	}

	//print when connecting to the server
	public void Connecting(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Connect received: " + e.name + " " + e.data);
	}
		
	//method which adds or update an object
	public void GetObject(SocketIOEvent e) 
	{
		Debug.Log("[SocketIO] GetObject received: " +e.data);
		try 
		{
			string str = e.data.Print();
			ObjectInformations objectInformations = new ObjectInformations();
			objectInformations = JsonUtility.FromJson<ObjectInformations>(str);
			Debug.Log("bloublou "+str);

			//updating position of object
			if(objectInformations.action.Equals("UPDATE")) 
			{
				GameObject o;
				gameObjects.TryGetValue(objectInformations.id_object.ToString(),out o);

				o.transform.position = new Vector3(objectInformations.position_x,
					objectInformations.position_y,
					objectInformations.position_z);
				return;
			}
			else if(objectInformations.action.Equals("RESET")) 
			{
				Debug.Log("HELLOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
				foreach(GameObject o in objectsToBeFound) {
					Destroy(o);
				}
				objectsToBeFound = new List<GameObject>();

				foreach(KeyValuePair<string,GameObject> pair in gameObjects) {
					Destroy(pair.Value);
				}
				gameObjects = new Dictionary<string, GameObject>();
			}

			//adding an object according to its category and its name
			else if(objectInformations.action.Equals("ADD")) 
			{
				GameObject obj=new GameObject();
				if(objectInformations.type_category.Equals("trees")) 
				{
					/*if(objectInformations.type_name.Equals("olive_tree")) {
						obj = (GameObject) Instantiate(olive_tree);
						obj.transform.rotation = Quaternion.Euler(270.0f, obj.transform.rotation.y,obj.transform.rotation.z);
					}*/
					if(objectInformations.type_name.Equals("conifer"))
						obj = (GameObject) Instantiate(conifer_tree);
					if(objectInformations.type_name.Equals("broadleaf_tree"))
						obj = (GameObject) Instantiate(broadleaf_tree);
					
				}
				else if(objectInformations.type_category.Equals("tobefound")) 
				{
					if(objectInformations.type_name.Equals("present"))
					{
						obj = (GameObject)Instantiate(present);
						gameObjects.Add(objectInformations.id_object.ToString(),obj);
					}
					objectsToBeFound.Add(obj);
				}
				else if(objectInformations.type_category.Equals("building"))
				{
					if(objectInformations.type_name.Equals("village_house_v1"))
						obj = (GameObject) Instantiate(village_house_v1);
					if(objectInformations.type_name.Equals("village_house_v2"))
						obj = (GameObject) Instantiate(village_house_v2);
					if(objectInformations.type_name.Equals("village_house_v3"))
						obj = (GameObject) Instantiate(village_house_v3);
					if(objectInformations.type_name.Equals("village_house_v4"))
						obj = (GameObject) Instantiate(village_house_v4);
					if(objectInformations.type_name.Equals("village_windmill"))
						obj = (GameObject) Instantiate(village_windmill);

				}
				else if(objectInformations.type_category.Equals("ambiance") && objectInformations.type_name.Equals("hail")) 
				{
					Debug.Log("The hail is moving wooooh");
					obj = (GameObject)Instantiate(hail, new Vector3(objectInformations.position_x,
						objectInformations.position_y,
						objectInformations.position_z), Quaternion.identity);
					obj.tag= objectInformations.id_object.ToString();
					gameObjects.Add(objectInformations.id_object.ToString(),obj);
				}

				obj.transform.position = new Vector3(objectInformations.position_x,
					objectInformations.position_y,
					objectInformations.position_z);

				obj.transform.rotation = Quaternion.Euler(obj.transform.rotation.x,objectInformations.angle,obj.transform.rotation.z);
				gameObjects.Add(objectInformations.id_object.ToString(),obj);

				gameObjects.Add(objectInformations.id_object.ToString(),obj);

			}
		}
		catch(Exception exc) 
		{
			Debug.Log(exc.ToString());
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

public class TestConnexion : MonoBehaviour
{
	private SocketIOComponent socket;

	public void Start() 
	{
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();

		socket.On("connect", TestConnect);
		socket.On("res", TestRes);
		
		StartCoroutine("Blou");
	}

	private IEnumerator Blou()
	{
		// wait 1 seconds and continue
		yield return new WaitForSeconds(1);
		
		Dictionary<string, string> data = new Dictionary<string, string>();
       	data["email"] = "some@email.com";
		socket.Emit("req",new JSONObject(data));
	}

	public void TestConnect(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Coonect received: " + e.name + " " + e.data);
	}
	
	public void TestRes(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Res received: " + e.name + " " + e.data);

		if (e.data == null) 
		{
			Debug.Log("BLOUBLOUBLOU"); 
			return; 
		}
	}
}
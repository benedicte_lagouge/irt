import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Platform, AlertController } from 'ionic-angular';
import { Push } from "ionic-native";
import * as io from 'socket.io-client';

import { HomePage } from '../pages/home/home';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class UserService {
  public url:string = "http://192.168.1.8:8082" // 11 - 8
  private socket;

  public initialDate: number = null;
  public status:UserStatus = UserStatus.progress;
  public distanceDataPrinted = [{data:[], label:"distance", selected:0, maxSelector:0}]; // user's data for distance between user and cadeau
  public distanceData = [[]]; // user's data for distance between user and cadeau
  public ddl = 0; //this.distanceData[0].data.length
  public distanceDataLabel:string[] = [];
  public distanceDataSheet:number = 1;
  public distanceDataPrevious = null;
  public distanceDataMoyenne = 0;
  public DISTANCE_DATA_LENGTH:number = 150;

  public userPosition: {x:number, y:number, z:number}[] = [{x:0, y:0, z:0}];
  public cadeauPosition: {x:number, y:number, z:number}[] = [{x:0, y:0, z:0}];

  public presentsIds: any[] = [];

  constructor(public http:Http, public platform: Platform, public alertCtrl: AlertController) {
    console.log("contact server at ", this.url);
    for (let i = 0; i < this.DISTANCE_DATA_LENGTH; i++)
      this.distanceDataLabel.push(i + '');

    this.listenServer().subscribe(message => {
    });
  }


  private homePage:HomePage;
  getUsers(getUserResponse):any {
    this.homePage = getUserResponse;
    this.socket.emit('accounts_get');
  }

  getUser(user:string, getUserResponse):any {
    this.homePage = getUserResponse;
    this.socket.emit('accounts_get', {user});
  }

  private isPresentIds(id: any): boolean {
    for (var i = this.presentsIds.length - 1; i >= 0; i--) {
      if(this.presentsIds[i] == id)
        return true;
    }
    return false;
  }

  private listenServer():Observable<any> {
    let observable:Observable<any> = new Observable(observer => {
      this.socket = io(this.url);
      // connection with server
      this.socket.emit('register', {'type': 'tablet'});

      // message's reception
      // player old result and info
      this.socket.on('accounts_get_result', (data) => {
        console.log("see result to accounts_get_result : ", data);
        this.homePage.setUsers(data);
        this.initAfterIoConnected();
      });

      // player currently status
      this.socket.on('distance', (data) => {
        console.log("see result to distance : ", data);
        this.saveDistanceData(data); 
        this.interpretUserDistanceData(data.distance);
        console.log("     status : ", this.status);
      });

      this.socket.on('position', (data) => {
        console.log("see result to position : ", data);
        this.userPosition[0].x = parseInt(data.x);
        this.userPosition[0].y = parseInt(data.y);
        this.userPosition[0].z = parseInt(data.z);
      });

      this.socket.on('object', (data) => {
        console.log("see result to object : ", data);
        if(data.type_name == "present"){
          if(! this.isPresentIds(data.id)){
            this.presentsIds.push(data.id);
            console.log("ADD PRESENT NB: ", data.id);
          }

          this.cadeauPosition[0].x = parseInt(data.position_x);
          this.cadeauPosition[0].y = parseInt(data.position_y);
          this.cadeauPosition[0].z = parseInt(data.position_z);
        } else {
          console.log("received update obj: ", data.id);
          if(this.isPresentIds(data.id)) {
            console.log("==update position of present");
            this.cadeauPosition[0].x = parseInt(data.position_x);
            this.cadeauPosition[0].y = parseInt(data.position_y);
            this.cadeauPosition[0].z = parseInt(data.position_z);
          }
        }
      });

      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  /**
   *  Is launched when io is registered, and first data set
   */
  private initAfterIoConnected(){
    // we set up Notification 
    this.platform.ready().then(() => {
      console.log("platform is ready");

      // find what is this ? FIXME
      // StatusBar.styleDefault();
      // Splashscreen.hide();
      this.initPushNotification();
    });
  }

  private saveDistanceData(data) {
    if (this.initialDate == null){
      this.initialDate = data.date;
      this.status = UserStatus.progress;
    }

    let date = data.date - this.initialDate;
    console.log("data.date - this.initialDate = ", date);
    // if there is a pause of 3s
    if(this.distanceDataPrevious != null)
      if ((data.date - this.distanceDataPrevious) > 3000) {
        date = 0;
        // we reset all datas
        this.distanceDataSheet = 1;
        this.distanceDataPrevious = null;
        this.distanceDataMoyenne = 0;
        this.initialDate = data.date;
        this.status = UserStatus.progress;
        this.distanceData[0] = [];
        this.distanceDataPrinted[0].data = [];
        this.ddl = 0;
      }

    this.distanceDataMoyenne = ((this.distanceDataMoyenne * this.ddl) + data.distance) / (this.ddl + 1);

    this.distanceData[0].push(data.distance);  
    this.distanceDataPrevious = data.date;

    // increment distanceDataSheet
    this.ddl = this.distanceData[0].length;
    if((this.ddl % this.DISTANCE_DATA_LENGTH) == 0)
      this.distanceDataSheet++;


    let currentSelectedValues = Math.floor(this.ddl / this.DISTANCE_DATA_LENGTH);
    if(this.distanceDataPrinted[0].selected == currentSelectedValues) {
      let dd = JSON.parse(JSON.stringify(this.distanceDataPrinted));
      dd[0].data.push(data.distance);
      this.distanceDataPrinted = dd;
    }
    if((this.ddl % this.DISTANCE_DATA_LENGTH) == 0){
      this.distanceDataPrinted[0].maxSelector++;
    }
  }

  private tilEq(a:number, b: number):boolean{
    return ((a + 2 > b) && (a - 2 < b));
  }

  private transitToBlocked(): boolean {
    if(this.ddl < 11)
      return false;
    for(let i = this.ddl - 1, stop = this.ddl - 11; i > stop; i-- ){
      if(! this.tilEq(this.distanceData[0][this.ddl - 1], this.distanceData[0][i]))
        return false;
    }
    this.status = UserStatus.blocked;
    console.log("change status to blocked: ", UserStatus.blocked);
    return true;
  }

  private LOST_LENGTH = 30;
  private transitToLost(): boolean {
    if(this.ddl <= this.LOST_LENGTH)
      return false;
    let sum = 0;
    for(let i = this.ddl - 1, stop = this.ddl - (this.LOST_LENGTH + 1); i > stop; i-- )
      sum += this.distanceData[0][i];
    let m = sum / this.LOST_LENGTH;
    if(m > this.distanceDataMoyenne){
      this.status = UserStatus.lost;
      return true
    }
    return false;
  }

  private resetStatus(distance): boolean{
    if (this.transitToBlocked())
      return true;
    if (this.transitToLost()) 
      return true;
    return false;
  }

  private interpretUserDistanceData(distance) {
    switch(this.status){
      case UserStatus.blocked: 
        console.log("isBlocked");
        if(! this.tilEq(this.distanceData[0][this.ddl - 1], this.distanceData[0][this.ddl - 2]))
          if(! this.resetStatus(distance))
            this.status = UserStatus.progress;
      break;

      case UserStatus.progress: this.resetStatus(distance); break;

      case UserStatus.lost:
        if(this.distanceData[0][this.ddl - 1] <= this.distanceData[0][this.ddl - 2])
          if(! this.resetStatus(distance))
            this.status = UserStatus.progress;
      break;
    }
  }

  public sendCommand(n:number) {
    this.socket.emit('push', {'message':n});
  }


  initPushNotification(){
    if (!this.platform.is('cordova')) {
      console.warn("Push notifications not initialized. Cordova is not available - Run in physical device");
      return;
    }
    console.log("platform is valid, we set up notifications.");

    let push = Push.init({
      android: {
        senderID: "15065673208"
      },
      ios: {
        alert: "true",
        badge: false,
        sound: "true"
      },
      windows: {}
    });

    push.on('registration', (data) => {
      console.log("device token ->", data.registrationId);

      this.socket.emit('devices_add', {'id':data.registrationId});
    });
    push.on('notification', (data) => {
      console.log('receive notifications message', data.message);

      // let self = this;
      //if user using app and push notification comes
      if (data.additionalData.foreground) {
        // if application open, show popup
        let confirmAlert = this.alertCtrl.create({
          title: data.title,
          message: data.message,
          buttons: [{
            text: 'Ok',
            role: 'cancel'
          }
          // , {
          //   text: 'View his result',
          //   handler: () => {
          //     console.log("click on 'View' button");
          //     //TODO: Your logic here
          //     // self.navCtrl.push(StatusPage, {user: data.message});
          //   }
          // }
          ]
        });
        confirmAlert.present();
      } else {
        //if user NOT using app and push notification comes
        //TODO: Your logic on click of push notification directly
        // self.navCtrl.push(DetailsPage, {message: data.message});
        console.log("Push notification clicked");
      }
    });
    push.on('error', (e) => {
      console.log(e.message);
    });
  }
}

export enum UserStatus {
  progress,
  notPerfect,
  blocked,
  lost
}


import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
// import { UserService } from '../../providers/user-service';

@Component({
  selector: 'page-charts-sessions',
  templateUrl: 'chart-by-session.html'
})
export class ChartBySessionPage {	
	public user;
	public date = "";

  constructor(public navCtrl: NavController, private navParams: NavParams) {
    this.user = navParams.get('name');
    this.date = navParams.get('date');
    console.log("view ", this.date);
  }
}
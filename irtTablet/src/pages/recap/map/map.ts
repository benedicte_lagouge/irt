import { Component } from '@angular/core';
import { UserService } from '../../../providers/user-service';


@Component({
  selector: 'mapComponent',
  templateUrl: 'map.html'
})
export class MapComponent {

	constructor(public userService: UserService) {

	}

	calculate(n: number):string {
		return (n + 50) * 4  + 'px';
	}
}
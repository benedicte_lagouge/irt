import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../providers/user-service';
import { ChartBySessionPage } from '../chartbysessions/chart-by-session'

@Component({
  selector: 'page-charts',
  templateUrl: 'charts.html'
})
export class ChartsPage {
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [
      {
        ticks: {
          beginAtZero: true,
          callback: label => {
            // console.log(label);
            return `${label.toFixed(2)}ms`;},
          id: "y-axis-0"
        }
      },
      {
        ticks: {
          beginAtZero: true,
          callback: label => `${label.toFixed(2)} m`,
          id: "y-axis-1"
        }
      }]
    }
  };

  public barChartLabels:string[] = [];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;

  public barChartDataLeft:any[] = [];
  public barChartDataRight:any[] = [];


  public userTimeLeft:any = {data:[], label:"Temps"};
  public userTimeRight:any = {data:[], label:"Temps"};
  public userLengthLeft:any = {data:[], label:"Distances"};
  public userLengthRight:any = {data:[], label:"Distances"};
  public emptyBar:any = {data:[], label:""};

  public loading: boolean = true;
  public state:{time:boolean, success:boolean} = {time:true, success:true}

  public user: any;

  constructor(public navCtrl: NavController, private navParams: NavParams, private userService: UserService) {
    this.loading = true;
    this.user = navParams.get('name');

    this.dodata(this.user.stats);

    this.userTimeLeft.yAxisID = "y-axis-0";
    this.userTimeRight.yAxisID = "y-axis-0";
    this.userLengthLeft.yAxisID = "y-axis-1";
    this.userLengthRight.yAxisID = "y-axis-1";

    this.doBarChartData();
    this.loading = false;
  }

  /**
   * ordonnate data received to valid charts data
   * separate in two parts: left and right, then separate in time (of user), 
   * distance (at initialize with goal) and session 
   */
  private dodata(stats:any[]) {
    // diff is difference between number of right and left
    let diff:number = 0;
    let date: Date;
    let prev: number = 0; 
    for(let stat of stats){
      console.log("elem: ", stat, "\n diff: ", diff);
      // we change of date
      if(prev != stat.date){
        // we add more with 'diff'
        if (diff > 0)
          for(let i = 0; i < diff; i++){
            this.userTimeLeft.data.push(0);
            this.userLengthLeft.data.push(0);
          }
        else 
          for(let i = 0; i > diff; i--){
            this.userTimeRight.data.push(0);
            this.userLengthRight.data.push(0);
          }
        date = new Date(stat.date);
        prev = stat.date;
      }
      // we add a new element
      if(stat.position == "left"){
        diff++;
        this.userTimeLeft.data.push(stat.time.toString());
        this.userLengthLeft.data.push(stat.distance);
      }else {
        diff--;
        this.userTimeRight.data.push(stat.time.toString());
        this.userLengthRight.data.push(stat.distance);
      }
      console.log(" and so ? ", this.barChartLabels.length, " ", this.userTimeLeft.data.length, " ", this.userTimeRight.data.length)
      if(this.userTimeLeft.data.length > this.barChartLabels.length || this.userTimeRight.data.length > this.barChartLabels.length) {
        this.barChartLabels.push(date.getDay().toString() + "/" + date.getMonth().toString() + "/" + date.getFullYear().toString());
        this.emptyBar.data.push(0);
      }
    }
  }


  // events
  public chartClicked(e:any):void {
    console.log(e);
    if (e.active.length > 0){
      this.navCtrl.push(ChartBySessionPage, {
        name: this.user,
        // date: e.active.
      });      
    }
  }

  public chartHovered(e:any):void {
    // console.log(e);
  }

  // manage buttons
  public resetSelection(){
    this.state.time = true;
    this.state.success = true;
    this.doBarChartData();
  }

  public setTime(){
    this.state.time = true;
    this.state.success = false;
    this.doBarChartData();
  }

  public setSuccess(){
    this.state.success = true;
    this.state.time = false;
    this.doBarChartData();
  }

  // ordonnate data for bar chart with 'state' value
  private doBarChartData() {
    this.loading = true;
    if(this.state.time && this.state.success){
      this.barChartDataLeft = [this.userTimeLeft, this.userLengthLeft];
      this.barChartDataRight = [this.userTimeRight, this.userLengthRight];
    }
    else if(this.state.time){
      this.barChartDataLeft = [this.userTimeLeft, this.emptyBar];
      this.barChartDataRight = [this.userTimeRight, this.emptyBar];
    }
    else if(this.state.success){
      this.barChartDataLeft = [this.emptyBar, this.userLengthLeft];
      this.barChartDataRight = [this.emptyBar, this.userLengthRight];
    }
    else{
      this.resetSelection();
      return;
    }
    this.loading = false;
  }
}

// export class Stat {
//   public date:number; 
//   public distance:number; 
//   public time:number;
// }

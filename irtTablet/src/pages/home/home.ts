import { Component, OnInit } from '@angular/core';

import { NavController } from 'ionic-angular';
import {ChartsPage} from "../charts/charts";
import { UserService } from '../../providers/user-service';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  public users:any[] = [];
  public loading : boolean = true;

  constructor(public navCtrl: NavController, public userService: UserService) {
    console.log("constructor")
  }

  ngOnInit (){
    console.log("init !", new Date());
    this.userService.getUsers(this);
  }

  setUsers(data) {
    console.log("result get ! ", data);
    this.users = data;

    for(let u of this.users){
      console.log("- ", u.name);
    }

    this.loading = false;
  }

  goto(user:any){
    this.navCtrl.push(ChartsPage, {
      name: user
    });
  }
}

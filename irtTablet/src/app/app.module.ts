import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusPage } from '../pages/recap/status';
import { MapComponent } from '../pages/recap/map/map';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { ChartsPage } from '../pages/charts/charts';
import { ChartBySessionPage } from '../pages/chartbysessions/chart-by-session';
import { TabsPage } from '../pages/tabs/tabs';

import { UserService } from '../providers/user-service';


import { ChartsModule } from 'ng2-charts';


// import '../node_modules/chart.js/dist/Chart.bundle.min.js';
// import 'Chart.bundle.min.js';

@NgModule({
  declarations: [
    MyApp,
    StatusPage,
    ContactPage,
    HomePage,
    ChartsPage,
    ChartBySessionPage,
    TabsPage,
    MapComponent
  ],
  imports: [
    ChartsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    StatusPage,
    ContactPage,
    HomePage,
    ChartsPage,
    ChartBySessionPage,
    TabsPage
  ],
  providers: [
    UserService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

/**
 * Created by root on 20/01/17.
 */

var mongodb = require('mongodb');

var dbmanager = {maps: {}, accounts: {}, devices: {}};

// Local mongodb url
var db_url = 'mongodb://localhost:27017/bavedb';

/** ----- MAPS ----- */
dbmanager.maps.get = function (callback, filter, test) {
  /** CORE **/
  // default filter parameter
  filter = typeof filter !== 'undefined' ? filter : {};
  // function
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('maps');
      collection.find(filter).toArray(function (err, result) {
        if (err) {
          console.log('Unable to get maps', err);
          return callback(false);
        } else {
          return callback(result);
        }
      });
    }
  });
};

dbmanager.maps.add = function (map, callback, test) {
  /** CORE **/
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('maps');
      collection.insertOne(map, function (err, result) {
        if (err) {
          console.log('Unable to add a map', err);
          return callback(false);
        } else {
          return callback(result);
        }
      });
    }
  });
};

dbmanager.maps.addObject = function (object, map_id, callback, test) {
  /** CORE **/
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('maps');
      collection.update({_id: ObjectId(map_id)}, {"$push": {"objects": object}},
        function (err, result) {
          if (err) {
            console.log('Unable to add an object to the map', err);
            return callback(false);
          } else {
            return callback(result);
          }
        });
    }
  });
};

dbmanager.maps.removeObject = function (object_id, map_id, callback, test) {
  /** CORE **/
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('maps');
      collection.update({_id: ObjectId(map_id)}, {"$pull": {"objects.id_object": ObjectId(object_id)}},
        function (err, result) {
          if (err) {
            console.log('Unable to remove an object from the map', err);
            return callback(false);
          } else {
            return callback(result);
          }
        }
      );
    }
  });
};

dbmanager.maps.remove = function (map_id, callback, test) {
  /** CORE **/
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(connection, function (err, db) {
      if (err) {
        console.log('Unable to connect to the mongodb server', err);
        return callback(false);
      } else {
        var collection = db.collection('maps');
        collection.remove({_id : ObjectId(map_id)}, {justOne: true}, function (err, result) {
          if (err) {
            console.log('Unable to remove map', err);
            return callback(false);
          } else {
            return callback(result);
          }
        });
      }
    });
};

/** ----- ACCOUNTS ----- */
dbmanager.accounts.get = function (callback, filter, test) {
  /** CORE **/
  // default filter parameter
  filter = typeof filter !== 'undefined' ? filter : {};
  // function
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('accounts');
      collection.find(filter).toArray(function (err, result) {
        if (err) {
          console.log('Unable to get accounts', err);
          return callback(false);
        } else {
          return callback(result);
        }
      });
    }
  });
};

dbmanager.accounts.add = function (account, callback, test) {
  /** CORE **/
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('accounts');
      collection.insertOne(map, function (err, result) {
        if (err) {
          console.log('Unable to add an account', err);
          return callback(false);
        } else {
          return callback(result);
        }
      });
    }
  });
};

dbmanager.accounts.addStat = function (stat, account_id, callback, test) {
  /** CORE **/
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('maps');
      collection.update({_id: ObjectId(account_id)}, {"$push": {"stats": stat}},
        function (err, result) {
          if (err) {
            console.log('Unable to add a stat to the account', err);
            return callback(false);
          } else {
            return callback(result);
          }
        });
    }
  });
};

dbmanager.accounts.removeStat = function (stat_date, account_id, callback, test) {
  /** CORE **/
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('maps');
      collection.update({_id: ObjectId(account_id)}, {"$pull": {"stats.date": stat_date}},
        function (err, result) {
          if (err) {
            console.log('Unable to remove a stat from the account', err);
            return callback(false);
          } else {
            return callback(result);
          }
        }
      );
    }
  });
};

dbmanager.accounts.remove = function (account_id, callback, test) {
  /** CORE **/
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(connection, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('accounts');
      collection.remove({_id : ObjectId(account_id)}, {justOne: true}, function (err, result) {
        if (err) {
          console.log('Unable to remove account', err);
          return callback(false);
        } else {
          return callback(result);
        }
      });
    }
  });
};

/** ----- DEVICES ----- */
dbmanager.devices.get = function (callback, filter, test) {
  /** CORE **/
  // default filter parameter
  filter = typeof filter !== 'undefined' ? filter : {};
  // function
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('devices');
      collection.find(filter).toArray(function (err, result) {
        if (err) {
          console.log('Unable to get devices', err);
          return callback(false);
        } else {
          return callback(result);
        }
      });
    }
  });
};

dbmanager.devices.exists = function (filter, callback, test) {
  /** CORE **/
  // default filter parameter
  filter = typeof filter !== 'undefined' ? filter : {};
  // function
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('devices');
      collection.find(filter).toArray(function (err, result) {
        if (err) {
          console.log('Unable to get devices', err);
          return callback(false);
        } else {
          return callback(result.length);
        }
      });
    }
  })
};

dbmanager.devices.add = function (map, callback, test) {
  /** CORE **/
  var MongoClient = mongodb.MongoClient;
  MongoClient.connect(db_url, function (err, db) {
    if (err) {
      console.log('Unable to connect to the mongodb server', err);
      return callback(false);
    } else {
      var collection = db.collection('devices');
      collection.insertOne(map, function (err, result) {
        if (err) {
          console.log('Unable to add a device', err);
          return callback(false);
        } else {
          return callback(result);
        }
      });
    }
  });
};


module.exports = dbmanager;
/**
 * Created by user on 15/12/2016.
 */
 
// We need to use the express framework: have a real web servler that knows how to send mime types etc.
var express=require('express');

// Init globals variables for each module required
var app = express()
    , http = require('http')
    , server = http.createServer(app)
    , io = require('socket.io').listen(server)
    , gcm = require('node-gcm')

    , querystring = require('querystring');

var dbmanager = require("./dbmanager");

var port = 8082;
var tableSocketId = null;
var vrSocketIds = [];
var tabletsSocketIds = [];


// launch the http server on given port
server.listen(port);
console.log("Listening on port " + port);

// Indicate where static files are located. Without this, no external js file, no css...
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.static(__dirname + '/'));

// routing
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/cli.html');
});

app.get('/table', function (req, res) {
  res.sendFile(__dirname + '/table-cli.html');
});

app.get('/vr', function (req, res) {
  res.sendFile(__dirname + '/vr-cli.html');
});

app.get('/push', function (req, res) {
  sendNotification("hello", "my first notification");
})


// Find in user status 
var distanceData = [];
var distanceDataMoyenne = 0;

var userStatus = 0;
var prevUserStatus = 0;
var ddl = 0;
var LOST_LENGTH = 30;
var BLOCKED_LENGTH = 11;

var tilEq = function(a, b){
  return ((a + 2 > b) && (a - 2 < b));
}

var transitToBlocked = function() {
  if(ddl <= BLOCKED_LENGTH)
    return false;
  var ddlp = ddl - 1;
  for(var i = ddlp, stop = ddl - BLOCKED_LENGTH; i > stop; i-- ){
    if(! tilEq(distanceData[ddlp], distanceData[i]))
      return false;
  }
  userStatus = 1;
  return true;
}

var transitToLost = function() {
    if(ddl <= LOST_LENGTH)
      return false;
    var sum = 0;
    for(var i = ddl - 1, stop = ddl - (LOST_LENGTH + 1); i > stop; i-- )
      sum += distanceData[i];
    var m = sum / LOST_LENGTH;
    if(m > distanceDataMoyenne){
      userStatus = 2;
      return true
    }
    return false;
  }

var resetStatus = function(distance){
  if (transitToBlocked())
    return true;
  if (transitToLost()) 
    return true;
  return false;
}

var interpretUserDistanceData = function(distance) {
  switch(userStatus){
    // 0 is in pregress
    case 0: resetStatus(distance); break;

    // blocked
    case 1: 
      if(tilEq(distanceData[ddl - 1], distanceData[ddl - 2]))
        return;
      if(! resetStatus(distance))
        userStatus = 0;
    break;

    // lost
    case 2:
      if(distanceData[ddl - 1] < distanceData[ddl - 2])
        if(! resetStatus(distance))
          userStatus = 0;
    break;
  }
}



io.sockets.on('connection', function (socket) {

  console.log("Connection [" + socket.id + "]");


  /** ----- REGISTER ----- */

  socket.on('register', function (data) {
    // io.sockets.connected[socketId].emit()

    if (data.type == "vr") {
      console.log("Registering new VR [" + socket.id + "]");
      vrSocketIds.push(socket.id);
    } else if (data.type == "tablet"){
      console.log("Registering new tablet [" + socket.id + "]");
      tabletsSocketIds.push(socket.id);
    }
    else if (data.type == "table")
    {
      console.log("Registering new table [" + socket.id + "]")
      tableSocketId = socket.id;
    } else
    {
      console.log("[register] Unknown type (invalid parameter)");
    }
  });

  /** ----- ACTIONS FROM TABLE ----- */

	// Object - Routing data Table --> VR(s) and tablet(s)
  socket.on('object', function (data) {
    console.log("Received object from [" + socket.id + "] : " + JSON.stringify(data));
    console.log("Sending object to " + vrSocketIds.length + " VR(s)");

    vrSocketIds.forEach(function(sId) {
      if (io.sockets.connected[sId] != null)
		io.sockets.connected[sId].emit("object", data);
    });

    tabletsSocketIds.forEach(function(sId) {
      io.sockets.connected[sId].emit("object", data);
    });
  });

	// Map - Routing data Table --> VR(s) (and save to DB)
  socket.on('map', function (data) {
    console.log("Received map from [" + socket.id + "] : " + JSON.stringify(data));
    console.log("Sending map to " + vrSocketIds.length + " VR(s)");

    // send to connected VR(s)
    vrSocketIds.forEach(function(sId) {
		if (io.sockets.connected[sId] != null)
			io.sockets.connected[sId].emit("map", data);
    });

    // save to DB
    dbmanager.maps.add(data, function(done){
      socket.emit('maps_add_result', done)
    });    
  });

	// POSITION - Routing data VR --> Table, tablet(s) and others VR(s)
  socket.on('position', function (data) {
    console.log("Received position from [" + socket.id + "] : " + JSON.stringify(data));
  
    data.from = socket.id;

    // Table
    if (tableSocketId != null) {
      console.log("Sending position to the table [" + tableSocketId + "]");
      io.sockets.connected[tableSocketId].emit("position", data);
    }

    // other VRs
    vrSocketIds.forEach(function(sId) {
        if (sId != socket.id)
          io.sockets.connected[sId].emit("position", data);
    });

    // all tablets
    tabletsSocketIds.forEach(function(sId) {
      io.sockets.connected[sId].emit("position", data);
    });
  });

  socket.on('distance', function(data) {
    // {distance: number, date: string }
    console.log("received distance: ", data);
    tabletsSocketIds.forEach(function(sId) {
      io.sockets.connected[sId].emit("distance", data);
    });
    distanceDataMoyenne = ((distanceDataMoyenne * ddl) + data.distance) / (ddl + 1);

    distanceData.push(data.distance);
    ddl++;
    interpretUserDistanceData();
    if((prevUserStatus != userStatus) && (userStatus > 0)){
      sendNotification("Problem", userStatus == 1 ? "Bénédicte is blocked": "Bénédicte is lost");
    }
    console.log("current: ", userStatus, "\nprevUserStatus: ", prevUserStatus);
    prevUserStatus = userStatus;
  })

// Stats - Routing data Table --> Tablet
  socket.on('stats', function (data) {
    console.log("Received stats from [" + socket.id + "] : " + JSON.stringify(data));
    io.sockets.emit("stats", data);
  });

  /** ----- UNREGISTER ----- */

  socket.on('disconnect', function(){
    console.log("Disconnection [" + socket.id + "]");

	// If socket disconnected is the table
    if (tableSocketId == socket.id)
    {
      tableSocketId = null;
    }
    else // Else a VR or a tablet ?  
    {
      var index = vrSocketIds.indexOf(socket.id);
      
      // If found
      if (index > -1) {
		  
		    // Remove VR socket from list
        vrSocketIds.splice(index, 1);
        
        var data = {};
        data.action = "REMOVE";
        data.from = socket.id;

        // Remove position of the VR in the table
        if (tableSocketId != null)
		{
			io.sockets.connected[tableSocketId].emit("position", data);
		}

        vrSocketIds.forEach(function(sId) {
          if (sId != socket.id && io.sockets.connected[sId] != null)
            io.sockets.connected[sId].emit("position", data);
        });    
      } else {
        // part about Tablets
        var index = tabletsSocketIds.indexOf(socket.id);
        if (index > -1) {
          tabletsSocketIds.splice(index, 1)
        }
        else
        {
          console.log("Unknown socket id");
        }
      }
    }
  });

  /** ----- DB ACCESS ----- */
  // MAPS
  socket.on('maps_get', function(data){
    dbmanager.maps.get(function(maps){
        socket.emit('maps_get_result', maps)
      }, (data || 0) ? data.filter : {});
  });

  socket.on('maps_add', function(data){
    dbmanager.maps.add(data.map, function(done){
      socket.emit('maps_add_result', done)
    });
  });

  socket.on('maps_object_add', function(data){
    dbmanager.maps.addObject(data.object, data.map_id, function(done){
      socket.emit('maps_object_add_result', done);
    });
  });

  socket.on('maps_object_remove', function(data){
    dbmanager.maps.removeObject(data.object_id, data.map_id, function(done){
      socket.emit('maps_object_remove_result', done);
    });
  });

  socket.on('maps_remove', function(data){
    dbmanager.maps.remove(data.map_id, function(done){
      socket.emit('maps_remove_result', done);
    });
  });
  
  // ACCOUNTS
  socket.on('accounts_get', function(data){
    dbmanager.accounts.get(function(accounts){
      console.log("get accounts PRODUCE RESULT");
      socket.emit('accounts_get_result', accounts)
    }, (data || 0) ? data.filter : {});
  });

  socket.on('accounts_add', function(data){
    dbmanager.accounts.add(data.account, function(done){
      socket.emit('accounts_add_result', done);
    });
  });

  socket.on('accounts_stat_add', function(data){
    dbmanager.accounts.addStat(data.stat, data.account_id, function(done){
      socket.emit('accounts_stat_add_result', done);
    });
  });

  socket.on('accounts_stat_remove', function(data){
    dbmanager.accounts.removeStat(data.stat_date, data.account_id, function(done){
      socket.emit('accounts_stat_remove_result', done);
    });
  });

  socket.on('accounts_remove', function(data){
    dbmanager.accounts.remove(data.account_id, function(done){
      socket.emit('accounts_remove_result', done);
    });
  });

  // DEVICES
  socket.on('devices_add', function (data) {
    console.log("tablet ", data.id, " connected");
    dbmanager.devices.exists( {"deviceId": data.id}, function(resultlength){
      if(resultlength === 0){
        console.log("Add it in DB");
        dbmanager.devices.add(data.id, function(done){
          deviceToken.push(data.id);
        });
      }
    });    
  });

  // TODO Tablets IDs  
  
  /** ----- DEBUG ----- */
  socket.on('req', function (data) {
    console.log(data);
    io.sockets.emit('res', data);
  });
});


var SERVER_KEY = "AAAAA4H77fg:APA91bElak98zrVOkm-pqeOoAHkHZu0vVcZ17DOhVcm1iWArkeT2vsg3kmu0Sq0qRku63TUcCQzkL-RTbEKV0aTYVjnFI-Wj_N8S_m-DcXu2h-o4KIgaxkSektDazQrsocyUTJAlqFv0";
var idNotifs = 1;
var deviceToken = [];

var sendNotification = function(title, mes) {
  console.log("send notification ", idNotifs++, ": ", title, "\nm: ", message);

  var retry_times = 4; //the number of times to retry sending the message if it fails
  var sender = new gcm.Sender(SERVER_KEY); //create a new sender
  var message = new gcm.Message(); //create a new message
  message.addData('title', title);
  message.addData('message', mes);
  message.addData('sound', 'default');
  message.collapseKey = 'User state'; //grouping messages
  message.delayWhileIdle = true; //delay sending while receiving device is offline
  message.timeToLive = 10; //number of seconds to keep the message on 
  //server if the device is offline
  
  dbmanager.devices.get(function(deviceTokenGetted){
    for (var i = deviceTokenGetted.length - 1; i >= 0; i--) {
      console.log("send to ", deviceTokenGetted[i].deviceId);
      sender.send(message, deviceTokenGetted[i].deviceId, retry_times, function (result) {
        console.log("success Pushed notification !");
      }, function (err) {
        console.log("failed to push notification...", err);
      });
    }
  });
}


